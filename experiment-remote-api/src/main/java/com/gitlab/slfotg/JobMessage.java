package com.gitlab.slfotg;

import java.io.Serializable;
import java.util.Map;

public class JobMessage implements Serializable {

    private static final long serialVersionUID = 1L;
    private String jobName;
    private Map<String, Object> parameters;

    public String getJobName() {
        return jobName;
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }

}
