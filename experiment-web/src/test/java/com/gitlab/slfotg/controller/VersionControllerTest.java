package com.gitlab.slfotg.controller;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gitlab.slfotg.domain.AppVersion;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class VersionControllerTest {

    @Autowired
    VersionController versionController;

    @Test
    public void testVersionController() {
        AppVersion appVersion = new AppVersion("0.0.1");
        assertThat(versionController.getVersion(), equalTo(appVersion));
    }
}
