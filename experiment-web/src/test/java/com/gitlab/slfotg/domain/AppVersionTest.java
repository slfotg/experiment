package com.gitlab.slfotg.domain;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class AppVersionTest {

    @Test
    public void testAppVersion() {
        String version = "1.2.3";
        AppVersion appVersion = new AppVersion(version);
        assertThat(appVersion.getVersion(), equalTo(version));
    }
}
