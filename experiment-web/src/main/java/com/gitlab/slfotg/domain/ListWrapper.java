package com.gitlab.slfotg.domain;

import java.util.ArrayList;
import java.util.List;

public class ListWrapper<T> {

    private List<T> items;

    public ListWrapper(List<T> items) {
        this.items = new ArrayList<>(items);
    }

    public List<T> getItems() {
        return items;
    }
}
