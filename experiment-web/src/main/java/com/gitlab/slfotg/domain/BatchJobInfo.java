package com.gitlab.slfotg.domain;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersValidator;
import org.springframework.batch.core.job.DefaultJobParametersValidator;

public class BatchJobInfo {

    private String jobName;
    private DefaultJobParametersValidator parameters;

    public BatchJobInfo(Job job) {
        if (job == null) {
            throw new NullPointerException();
        }
        jobName = job.getName();
        initParameters(job.getJobParametersValidator());
    }

    private void initParameters(JobParametersValidator jobParametersValidator) {
        if (jobParametersValidator == null) {
            return;
        }
        if (!(jobParametersValidator instanceof DefaultJobParametersValidator)) {
            return;
        }
        parameters = (DefaultJobParametersValidator) jobParametersValidator;
    }

    public String getJobName() {
        return jobName;
    }

    public DefaultJobParametersValidator getParameters() {
        return parameters;
    }

}
