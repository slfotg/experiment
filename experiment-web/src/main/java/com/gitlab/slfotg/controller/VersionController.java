package com.gitlab.slfotg.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.slfotg.domain.AppVersion;

@RestController
@RequestMapping("/version")
public class VersionController {

    @GetMapping
    @ResponseBody
    public AppVersion getVersion() {
        return new AppVersion("0.0.1");
    }
}
