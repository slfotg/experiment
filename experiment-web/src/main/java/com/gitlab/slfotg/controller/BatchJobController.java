package com.gitlab.slfotg.controller;

import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.slfotg.domain.ListWrapper;
import com.gitlab.slfotg.domain.Person;
import com.gitlab.slfotg.service.BatchJobService;
import com.gitlab.slfotg.service.PersonService;

@RestController
@RequestMapping("/batch")
public class BatchJobController {

    @Autowired
    private BatchJobService batchJobService;

    @Autowired
    private PersonService personService;

    @GetMapping
    @ResponseBody
    public ListWrapper<String> getBatchJobNames() {
        return new ListWrapper<>(batchJobService.getBatchJobNames());
    }

    @GetMapping("/exec/{jobName}")
    @ResponseBody
    public Long executeJob(@PathVariable("jobName") String jobName) throws JobExecutionAlreadyRunningException,
            JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException {
        return batchJobService.launchJob(jobName);
    }

    @GetMapping("/people")
    @ResponseBody
    public ListWrapper<Person> getPeople() {
        return new ListWrapper<>(personService.getPeople());
    }
}
