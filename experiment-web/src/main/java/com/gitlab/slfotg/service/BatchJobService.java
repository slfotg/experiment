package com.gitlab.slfotg.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gitlab.slfotg.domain.BatchJobInfo;

@Service
public class BatchJobService {

    Logger logger = LoggerFactory.getLogger(BatchJobService.class);

    @Autowired(required = false)
    List<Job> jobs = new ArrayList<>();

    @Autowired(required = false)
    private JobLauncher jobLauncher;

    Map<String, Job> jobNameMap = new HashMap<>();

    @PostConstruct
    private void init() {
        logger.info("Initializing BatchJobService");
        jobs.forEach(job -> {
            logger.info("Found job: " + job.getName());
            jobNameMap.put(job.getName(), job);
        });
    }

    public List<String> getBatchJobNames() {
        return jobs.stream().map(job -> job.getName()).collect(Collectors.toList());
    }

    public List<BatchJobInfo> getJobInfos() {
        return jobs.stream().map(job -> new BatchJobInfo(job)).collect(Collectors.toList());
    }

    public Long launchJob(String jobName) throws JobExecutionAlreadyRunningException, JobRestartException,
            JobInstanceAlreadyCompleteException, JobParametersInvalidException {
        Job job = jobNameMap.get(jobName);
        JobExecution execution = jobLauncher.run(job, new JobParameters());
        return execution.getJobId();
    }
}
