package com.gitlab.slfotg.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gitlab.slfotg.domain.Person;

@Service
public class PersonService {

    @Autowired(required = false)
    List<Person> people = new ArrayList<>();

    public List<Person> getPeople() {
        return people;
    }

    @PostConstruct
    private void init() {
        people.forEach(System.out::println);
    }
}
