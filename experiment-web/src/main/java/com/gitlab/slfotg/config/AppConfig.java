package com.gitlab.slfotg.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.gitlab.slfotg.domain.Person;

@Configuration
public class AppConfig {

    @Bean
    public Person sam() {
        return new Person("Sam", "Foster");
    }

    @Bean
    Person katie() {
        return new Person("Katie", "Hopper");
    }

    @Bean
    Person hannah() {
        return new Person("Hannah", "Foster");
    }
}
