package com.gitlab.slfotg;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class JobMessageReceiver {

    @JmsListener(destination = "job")
    public void receiveJobMessage(JobMessage message) throws Exception {
        System.out.println("Received request to start job: " + message.getJobName());
    }
}
