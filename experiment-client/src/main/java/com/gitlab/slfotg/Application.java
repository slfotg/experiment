package com.gitlab.slfotg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.jms.core.JmsTemplate;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
        System.out.println("asdjkfasjd");
        JmsTemplate jmsTemplate = context.getBean(JmsTemplate.class);
        JobMessage message = new JobMessage();
        message.setJobName("test-job");
        jmsTemplate.convertAndSend("job", message);
        context.close();
    }
}
