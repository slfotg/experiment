package com.gitlab.slfotg;

import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;

//@SpringBootApplication
public class BatchApplication {
    
    @Autowired
    JobLauncher jobLauncher;

    public static void main(String[] args) throws Exception {
        SpringApplication.run(BatchApplication.class, args);
    }
}
